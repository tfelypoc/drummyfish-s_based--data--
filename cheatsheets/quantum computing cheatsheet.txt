tl;dr quantum computer:
  Uses quantum phenomena, most notably superposition, entanglement and
  interference, to accelerate (get better computational complxity) computations
  of specific problems, notably e.g. factoring large numbers (Shor's algorithm,
  security concern).

  The principle of acceleration is instead of performing computation on a single
  input to get single output, we can perform computation an a superposition of
  multiple inputs and effectively get them evaluated in parallel (acceleration).
  With this we get again a superposition of all possible outputs, each with a
  certain probabilty to be measured. When we measure the result, we randomly get
  one of these results. Interference can be exploited in the design of quantum
  algorithms so that the answers we're not interested in "cancel out" (get zero
  probability of being measured) so that we only get a result we're interested
  in.

For vectors here we use Dirac (bra-ket) notation: |a> means vector named "a".

qubit:
  Quantum bit, physically represented by some physics quantum property, e.g.
  spin. When measured, collapses into a result of either 1 or 0 (like bit), but
  until measured has a more complex internal state (can hold more than 1 bit of
  information). This state can be a superposition ("something between") of 1 and
  0 (implying a probability of either one). 

  The qubit state is a linear combination of two base vectors |0> = [1 0] and
  |1> = [0 1]:

  state = a * |0> + b * |1>

  Where a and b are complex numbers. |a|^2 gives the probability of measuring
  0, |b|^2 gives the probability of measurinf 1 (so |a|^2 + |b|^2 = 1).

  Using complex reasoning we can deduce that the whole qubit state can be 
  represented as a point on a surface of a 3D sphere (Bloch sphere):

          z
          
          ^  |0>
         _|_
      __/ | \__
    _/    |    \_
   /      | /q   \
   |      |/     |
  |       |-------|------ y
   |     /|      |
   \_   / |     _/
     \_/  |  __/
      / \_|_/
     /    |
    x     v  |1>

  Above the qubit state q can be represented by two angles: D (pitch) and P
  (yaw), and the complex numbers a and b can be computed as:

  a = cos(D / 2)                 (can be chosen to be always real)
  b = e^(i*F) * sin(D / 2)

quantum gate:
  Basic building block of a quantum circuit (most common quantum computation
  model).

  The number of input quibits, N, is always equal to the number of output
  qubits.

  
 
