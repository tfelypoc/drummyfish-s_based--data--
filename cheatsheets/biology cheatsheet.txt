CELL

  Basic unit of life, however not all life (e.g. viruses are non-cellular life).
  Cells are:

  - Prokaryotes: do NOT have nucleus, only nucleoid. Always form single cell
      organisms.
  - Eukaryotes: Have nucleus, can form single or multiple cell organisms.

HUMAN GENETICS/DNA:

The whole genetic information is called a genome. Each individual has a
different genome, which can also differ in size (about 1%).

Each cell contains 23 pairs of chromosomes in the nucleus, i.e. 46 total (this
is called "diploid") plus a small portion of mitochondrial DNA (mDNA) outside
the nucleus (16569 pairs, 13 proteins). Chromosomes are physically separated and
have an X shape. Each chromosome has its own DNA sequence. All these DNA
sequences together form the genome. Ideally all cells in an organism hold the
identical copy of genetic information.

Every chromosome pair except for the last one (23rd) consists of one chromosome
from the mother and one from the father and they have the same "format" (same
lengths, location of genes etc.). The 23rd pair is special, it's a "sex" pair:
there are two possible chromosome types in this pair: X and Y (smaller), males
have X from mother and Y from father, females have two Xs (one from mother and
one from father).

Each chromosome consists of one long DNA which is subdivided into genes, the
basic code units that encode individual proteins. A chromosome contains hundreds
to thousands of genes (total number of human genes is not known but it's in tens
of thousands). A gene is thousands pairs long.

DNA (deoxyribonucleic acid) is a big molecule consisting of 2 long chains
forming a double helix shape. It encodes and carries genetic infomration, a
"source code" of an organism. DNA is composed of pairs of nucleotides. There
can be 5 types of nucleotides in DNA:
  - A (adenin)
  - T (thymine)
  - C (cytosine)
  - G (guanine)

A only pairs with T, C only pair with G, so the possible pair in DNA are:
AT, TA, CG, GC, so the DNA code is in base 4.

DNA consists of two parts:

- coding part (< 2%): Encodes proteins (the organism build blocks). This part is
  subdivided into genes.
- non-coding part Doesn't encode proteins (but can have other functions, such
  as regulating, the part we don't know any function of is called "junk" DNA).

  


The total length of human genome (diploid, all chromosome DNAs combined) is
about 6.4 billion pairs, i.e. ~1.6 GB of information. The mapped human genome
(haploid) has 3088286401 pairs (including mDNA) and 20412 genes.
