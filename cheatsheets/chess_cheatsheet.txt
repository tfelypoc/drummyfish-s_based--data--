OPENINGS:
  e4        KING'S PAWN, most popular, kinda aggresive, frees queen and bishop
   e5       OPEN GAME (double king's pawn)
    Nf3     white attacks black center pawn
     Nc6    black protects black center pawn
      Bb5   RUY LOPEZ / SPANISH GAME, white attacks the protector of black center pawn
      d4    SCOTCH GAME
      Bc4   ITALIAN GAME
      Nc3
       Nf6  FOUR KNITS GAME
    Nf6     PETROV'S DEFENSE, black counter attacks white center pawn
     Nxe5   white takes the black center pawn, hoping black will counter-take white pawn: mistake because Qe2 Qe7 Qxe4 wins a knight
      Pd6   black attacks white knight who has to go back, then black can counter-take white pawn
    f4      KING'S GAMBIT
    Ke2     BONGCLOUD, meme opening, NOT good
   c5       SICILIAN DEFENSE
   e6       FRENCH DEFENSE
   c6       CARO-KANN DEFENSE
   d5       SCANDINAVIAN DEFENSE
   g6       MODERN DEFENSE
  d4        QUEEN'S PAWN
   d5       CLOSED GAME (double queen's pawn)
    c4      QUEEN'S GAMBIT
     xc4    QUEEN'S GAMBIT ACCEPTED
     e6     QUEEN'S GAMBIT DECLINED
     c6     SLAV DEFENSE
   Nf6
    c4
     g6     KING'S INDIAN DEFENSE (hypermodern opening)
   ?
    Nf3
     ?
      Bf4   LONDON SYSTEM
  g4        GROB'S OPENING, sometimes considered the worst first move, NOT good
