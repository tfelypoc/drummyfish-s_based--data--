uppercase  А  Б  В  Г  Ґ  Д  Є  Е  Ё  Ж  З  И  Й  I  Ї  К  Л  М  Н  О  П  Р  С  Т  У  Ф  Х  Ц  Ч  Ш  Щ  Ь  Ъ  Ы  Ь  Э  Ю  Я
lowercase  а  б  в  г  ґ  д  є  е  ё  ж  з  и  й  i  ї  к  л  м  н  о  п  р  с  т  у  ф  х  ц  ч  ш  щ  ь  ъ  ы  ь  э  ю  я 

RUS pron   a  b  v  g     d     je jo ž  z  i  j        k  l  m  n  o  p  r  s  t  u  f  ch c  č  š  šč       i     e  ju ja
UKR pron   a  b  v  h  g  d  je e     ž  z  i  j  i  ji k  l  m  n  o  p  r  s  t  u  f  ch c  č  š  šč                ju ja

CZ:
  same     X                                      X     X     X     X           X
  similar     X              X  X                                                                                   X
  different      X  X  X  X        X  X  X  X  X     X     X     X     X  X  X     X  X  X  X  X  X  X  X  X  X  X  X  X  X


Approximate algorithm to CZ pron:

  1. Try this mapping first:

    B     v
    P     r 
    H     n
    C     s
    y     u
    Ф     f  
    Х     ch
    Ц Ч   c (č)  
    Ш Щ   š (šč)
    Д     d
    Г Ґ   g (h)
    Ж     ž
    3     z 
    Л     l
    И Й   i (j)
    П     p
    Ю     ju
    Я     ja
    Ё     jo
 
  2. Looks like lowecase b? If the top is pointing right (Б, б),
     then it is b, else ignore the letter. 

  3. Now pick the most similar in CZ (e.g.: Є Е e Э => e).
    








