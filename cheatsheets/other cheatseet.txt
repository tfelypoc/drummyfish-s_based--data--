Cheatsheet of other things.

SCIENCE
-------

scientific method:
  One possible way of getting knowledge in science, based on empiricism
  (acquiring information through our senses).

  Other ways (than the scientific method) of acquiring truth in science exist,
  e.g. deductive reasoning from axioms, which is based on rationalism rather
  than empirism.

  Scientific method means usually working in these steps:

  1. Ask a question about the world.
  2. Form a hypothesis based on the question.
  3. Design an experiment to test the hypothesis.
  4. Carry out the experiment.
  5. Analyze the results and extract new knowledge from them.

theory:
  An explanation of an aspect of the real world that can be tested using the
  scientific method. A theory can't be proven, only disproved or supported by
  evidence, using inductive reasoning.

theorem:
  Statement which can be proven or disproven using deductive reasoning.

- science:
    Systematic effort that searches for and organizes verifiable knowledge which
    can be used to make predictions about the universe.

    Some sciences can fall under multiple categories (e.g. linguistics can be
    a social science but also a formal science, depending on the specific
    subfield).

    Science (and its research) is eiter basic (studies the basic natural laws)
    or applied (applies basic knowledge in practice, e.g. medicine, technology,
    ...).

  - strict sciences:
      Use very reliable methods, principle and tools (scientific method, 
      deductive reasoning, falsifyability, math, ...) of gaining knowledge.

    - formal sciences:
        Study the abstract formal languages of science, create tools to be used
        by other sciences, don't generally use scientific method and empiricism,
        though these can be used in their context too (e.g. "experiments with
        numbers"). Formal sciences produce theorems, which can (and should) be 
        completely proven. Use deductive reasoning.

        - mathematics
        - logic
        - statistics
        - theoretical computer science
        - information theory
        - theoretical linguistics

    - empirical sciences:
        Primarily use the scientific method, produce theories, which can't be
        proven, only disproved or supported by evidence. Use inductive
        reasoning.

      - natural sciences:
          Try to be as exact as possible, maximize objectivity and minimize the
          influence of the observer, are highly skeptical, require
          falsifiability. 

        - physics
        - chemistry
        - biology
        - medicine
        - earth sciences

      - social sciences:
          Study individuals and their relationships on a high level. While still
          using the scientific method and scientific principles, they allow
          other methods of gaining knowledge (speculation, ...), are not
          strictly exact, are more subjetive and allow the observer to interact
          with the subjects (which is often the only way to gain knowledge).

        - psychology
        - sociology
        - anthropology
        - history (partly)
        - archeology (partly)
        - economics
        - geography
        - linguistics
        - law

  - humanities:
      Study the aspects and products of human culture, do not use scientific
      method and so don't produce theories (nor theorems). The methods are
      speculative and critical.

      - philosophy
      - literature
      - religion
      - arts
      - history (partly)
      - archeology (partly)

- pseudoscience:
    Something that tries to look like strict science but doesn't fit the
    definition of it.

    - astrology:
      Does not correctly predict the world, doesn't use scientific method, often
      gives unfalsifyable theories.
    - numerology
    - alchemy
    - flat earth theory
    - homeopathy
