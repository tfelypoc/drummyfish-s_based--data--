ufdfd,
======
drummyfish-s_based--data--
------------

These are text data drummyfish keep. The data may be regarding drummyfish, things drummyfish'm interested in, they may be config files etc.

The data are supposed to be factual, mostly time-persistent and should still be mostly valid even if not updated for some time. Other kinds of data are to be stored elsewhere.

By text data, drummyfish mean:
+ mostly plain text
+ sometimes human readable text-based formats (HTML, MD, ...)
+ no binary formats

drummyfish hereby release everything in this repository under__CC0 1.__ (https://creativecommons.org/publicdomain/zero/1.0/), public domain. Feel free to use it in any way you like.