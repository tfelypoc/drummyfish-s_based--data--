WIP

This is my C99 formatting style guide.

GENERAL
-------

- Use empty spaces to nicely separate chunks of code that belongs together,
  e.g.:

    int a = x;
    char b = y;

    if (a > b)
      doSomething();

    if (something())
      doSomethingElse();

- Undef macros when no longer needed. This prevents clashes.

INDENTATION
-----------
  Use two (2) SPACES for indentation, NEVER use tabs.

  Reasons:
    - 1 space is too few, 3 is retarded, 4 is too many (makes code hard to keep
      in 80 columns). 2 is just perfect for people with normal sight.
    - Tabs look like spaces but aren't, they're very sneaky and messy and fuck
      up the formatting depending on how each editor handles them (how long?
      wrapping? etc.). They are non-standard characters that behave differently
      than any other character that just takes exactly 1 space. Tabs also get
      mixed with spaces when you want to align to non-tab width multiple. When
      tabs are on, editor does shit like automatically replace spaces with tabs,
      you don't want your editor to be smart and fuck up your code behind your
      back. Tabs force bloat in text editors. Tabs just bother people who don't
      use them and don't have them configured, this isn't he case the other way
      around (there isn't anyone who doesn't have spaces configured).

IDENTIFIERS
-----------
  Generally, for identifiers with big scope (globals etc.) use descriptive,
  self-documenting identifiers, e.g. "getTimeSecs" instead of "time". For
  smaller scope identifiers (e.g. local variable in a short function) short
  identifiers like "x", like in mathematical formulas, can be used as the
  programmer can see what's in the variable. The same goes for macro names.

  Variables and functions: camelSpace with lowercase first letter, e.g.:

    myVartiable
    myNiceFunction

  Data types: CamelSpace with uppercase first letter, e.g.:

    MyInt
    SceneArray

  In code that is meant to be reused elsewhere use a namespace prefix for glabal
  identifiers, typically formed by three uppercase letters (usually an
  abbreviation of project's name) followed by an underscore, e.g. for My Awesome
  Project a global identifier "var" would be:

    MAP_var

  Prefix private/internal global variables with underscore to avoid clashes with
  normal identifiers, e.g.:
  
    _MAP_privateVar

CURLY BRACKETS
--------------
  Write curly brackets like this:

    if (condition)
    {
      doA();
      doB();
    }

  Reasons:
    - Visually very well readable, same level brackets are aligned. The lines
      with brackets also create nice empty spaces separating chunks of code.
    - Results in more LOC, forcing the programmer a little more to write shorter
      code.

  If not required (a single command in block), don't use brackets, except with
  tricky structures (e.g. if-else inside an if).

LINE LENGTH
-----------
  Maximum line length: 80

  Reasons:
    - When using two columns in vim on smaller displays, 80 columns fit just
      nicely without overflow/wrapping.
    - When making a paper backup, 80 lines fit just nicely into two columns on
      A4 paper and use the page size very efficiently.
    - LOC will correlate better with code complexity, as with very long line can
      be worth many a shorter lines.
    - Forces the programmer a little bit to write shorter code as shorter lines
      result in more LOC and create pressure on keeping it shorter and write
      more compact expressions that fit to a single line.
    - This can make processing easier for simple compilers or text editors that
      have limited line length.
    - It just looks cool.

COMMENTS
--------
  Generally use comments to make each file self-documented, do NOT rely on
  other files to provide crucial info (like a license, author, ...).

  At the beginning of each file always put a comment with:

  - Short file description. In bigger files include also basic documentation,
    like API explanation, usage explanation etc.
  - Author, year of creation (for copyright).
  - Waiver of all copyright and intellectual property (at worst a permissive
    license).

  Use Doxygen style comments so that auto doc can easily be created.
