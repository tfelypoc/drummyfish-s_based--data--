This is a list of common faulty arguments I often hear, along with explanations
why they're false.

1. If we can't achieve perfection, we shouldn't try at all.

   e.g.: "People will never be completely equal, so communism won't work."

   We can practically never achieve anything perfectly, which however doesn't
   mean the effort is meaningless, because getting closer to an ideal goal is
   usually better than not getting closer to it. In fact, this is what we mostly
   aim for -- get as close to perfection as possible, even knowing perfection
   can't be achieved -- a great example of this is law, which we know will never
   be perfect, yet we also know we shouldn't abanndon the idea just because of
   this fact.

2. X is (un)natural (normal), therefore X is (un)desirable.

   This doesn't follow, because many things are natural and undesirable ("bad")
   and many other things are unnatural and desirable ("good") (the meaning of
   good/bad of course being dependent on the topic being discussed and further
   reasoning).

   Examples of potentially bad natural things: diseases, agony, bullying, ... 

   Examples of potentially good unnatural things: science, houses, books, ...

   So with this argument you have to argue further why te property of being
   (un)natural implies (un)desirability.

3. Ad hominem: arguing against (for) an author of a logical argument instead of
   against (for) the argument itself.

   E.g. "He is a pedophile, so his idea should be rejected."

   Any idea that is supposed to be logically verifiable, i.e. a rational logical
   argument, is completely independent of its source, i.e. where it comes from.
   The source can only matter when the information is not a logical argment,
   i.e. it cannot be logically verified, e.g. a leaked inside information,
   a rumor etc.

4. It is his property, therefore he can do anything he wants with it.

   E.g. "The social network runs on his server, therefore he can censor whatever
         he wants on it."

   This is false because the scope of property rights (both legal or moral), as
   of any rights, have limits. For example if someone owns a gun, it generally
   is not his right to point that gun to someone else and pull the trigger,
   i.e. to kill the person, or if someone owns a sound system, it is not his
   right to play loud music with it in the middle of the night.
   
  
