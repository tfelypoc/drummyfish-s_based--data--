  17.11.1989         Velvet Revolution
1990.08.24 11:45   born in Zlín, 2.95 kg, 49 cm
1990.09.29         welcomed as a new citizen in Babice
1990.10.13         first smile
1990.10.23         reached 5 kg
1990.11            first word: "agú"
1990.12            moved to a flat in Břeclav
--------------------------------------------------------------------------------
1991.01            started to eat vegetables
1991.03            stopped breast-feeding
1991.04.20         stood up on feet and and said "tata" (daddy)
1991.06.03         first tooth
1991.07.12         first step
  1991.09.19         Linux is released
1991.12.10         said "maminka" (mommy)
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
  1993.01.01         Czechoslovakia splits into Czech and Slovakia
1993               fever, went to hospital for a week
1993.07.16         brothers were born
  1993.12.10         Doom comes out
--------------------------------------------------------------------------------
  1994.02.04         Ace Ventura (1) premiers
1994.04            grandgather (in law, father's side) died
  1994.10.13         Pulp Fiction premiers
1994               moved to Mařatice flat (stayed with grandparents for a while)
--------------------------------------------------------------------------------
  1995.12            Dračí Historie released
  1995.09.29         Playstation released
--------------------------------------------------------------------------------
  1996.01.29         Duke Nukem 3D released
  1996               Warcraft II released
--------------------------------------------------------------------------------
  1997.07.05         the great flood of Moravia
1997               holiday in Biograd (Croatia), first time at seaside (house: 43.93840288370077, 15.45129809823071)
1997               watched the very bright comet Hale-Bopp
1997               1st PC, dad bought it from work, Windows 3.1
1997.09.01         started elementary school in Mařatice (1 year postponed)
--------------------------------------------------------------------------------
1998.01.23         mom has a car accident (will be OK)
  1998.02.05         Titanic premiers in Czech Republic
  1998.12.23         Gameboy Color released in Europe
1998               went to the cinema to see Titanic (with subtitles) with mom
--------------------------------------------------------------------------------
  1999.05.19         Star Wars (prequel 1) premiers
1999.08.11         watched the total solar eclipse
1999.08            moved to a house in Sušice
1999.09.01         transitioned to elementary school in Babice
  1999.12.02         Quake III Arena released
1999               ??? got first cell phone (Erisccon T10s)
--------------------------------------------------------------------------------
2000.01.01 00:00   celebrated new millenium at the square in Uherské Hradiště
  2000.06.16         Pokémon Yellow released in Europe
2000.08            holiday in Caorle (Italy) with family (camp: 45.61554435311908, 12.90369973720829), trip to Venice
--------------------------------------------------------------------------------
2001.06            holiday in Novalja (Croatia, Pag island) with family (house: 44.55988049838873, 14.875903659317986), extended 3 das in Privlaka (house: 44.2572756556983, 15.128398777235212)
  2001.06.22         Gameboy Advance released in Europe
  2001.09.11         9/11 terrorist attacks in the US
  2001.09.24         Windows XP released 
  2001.11.02         Pokémon Crystal released in Europe
  2001.11.16         Harry Potter (movie 1) premiers
  2001.12            Lord of the Rings (movie 1) premiers
-------------------------------------------------------------------------------- 
  2002.01.11         Advance Wars released in Europe
  2002.05.02         TES Morrowind released in Europe
  2002.05.16         Star Wars (prequel 2) premiers
  2002.07.05         Warcraft III released in Europe
2002.08            holiday in Gargano (Italy) with family
2002.08            bought Gameboy Color with Pokémon Yellow
2002.08.24         went to aunt Andrea's wedding
  2002.08.19         Mafia released
  2002.10.29         GTA Vice City released
  2002.11.15         Harry Potter (movie 2) premiers
  2002.12            Lord of the Rings (movie 2) premiers
--------------------------------------------------------------------------------
2003               got a dog: Endy (golden retriever)
  2003.03.07         Václav Klaus becomes the president of Czech Republic
  2003.03.25         Linkin Park release Meteora (album with the song Numb)
2003               holiday in Brela (Croatia) with family (house: TRGOVAČKI OBRT M B VL. JURICA CAREVIĆ BRELA)
  2003.06.25         Pokémon Sapphire released in Europe
  2003.10            Nokia 6600 released
2003.11            got small dogs: Matýsek (chihuahua) & Čiko (Prague ratler)
  2003.12            Lord of the Rings (movie 3) premiers
--------------------------------------------------------------------------------
  2004.05.01         Czech Republic enters the EU
2004.05            holiday in Bohemia (Čechy) with family, in Land Rover
  2004.05.31         Harry Potter (movie 3) premiers
2004.06            holiday in Brela (Croatia) with family
2004               2nd place in school's mathematical competition (Klokánek)
  2004.08.13         Doom 3 released in Europe
  2004.10.26         GTA San Andreas released
  2004.11.16         Half Life 2 is released
--------------------------------------------------------------------------------
2005.01.21         techer's reprimant (důtka tř. učitele) for neglecting school
  2005.02.11       World od Warcraft released in Europe
  2005.05.15       Star Wars (prequel 3) premiers
2005               holiday in Brela (Croatia) with family
  2005.11.18         Harry Potter (movie 4) premiers
--------------------------------------------------------------------------------
2006.01.30         great-grandmother (fron mother's side) dies
2006.02.11         attended final school ball (Polonéza)
2006.06.30         1st place in school's mathematical competition (Klokánek)
2006               Internet connection at home
2006.09.01         started attending high school: SPŠ Zlín
2006               written a short story at school, teacher liked it very much
2006.09.13         broken leg during phys. education at school
2006.09            started playing World of Warcraft on offic. server (Drummy)
2006.12.30         great grand mother (father's side) died
--------------------------------------------------------------------------------
  2007.01.16         WoW: TBC released in Europe
2007.04            holiday in Hurghada, Egypt , with family (place: 27.260314879580594, 33.82145067695522), trip to Giza (pyramids, 2007.04.05 10:00)
  2007.07.12         Harry Potter (movie 5) premiers
2007.07.21         ??? read the first book in English: Harry Potter 7
2007.08.25         in World of Warcraft took part in raid on Ironforge
--------------------------------------------------------------------------------
2008.06.17         went to Linkin Park concert in Brno
2008.07            holiday in Breal (Crotia) with family
2008.09.01         started officially learning programming at school (Pascal)
  2008.11.13         WoW: WotLK released in Europe
  2008.12.03         GTA IV released
--------------------------------------------------------------------------------
2009.02.14         attended brothers' final school ball (Polonéza)
2009.03.08         registered at Facebook
2009.06            holiday in Brela (Croatia) with family
2009.06.29         got driver's license
  2009.07.15         Harry Potter (movie 6) premiers
  2009.07.022        Windows 7 is released
  2009.12.10         Avatar premiers
--------------------------------------------------------------------------------
2010.03.22         accepted by Brno University of Technology
2010.05.17         accepted by Masaryk University
2010.05.01         started dating 1st girlfriend
2010.05.24         graduated high school (SPŠ Zlín), commendat. for final work
2010.08            holiday on Rab island (Croatia) with family
2010.09.01         started University: FIT BUT (Brno), Bachelor's IT program
--------------------------------------------------------------------------------
2011.08            holiday in České Švýcarsko with girlfriend and her family (visited Děčín at 2011.08.01)
2011.08            short summer "job", working for the Babice village
2011.09.10         took part in "Slavnosti Vína" (folk festival) with girlfriend
2011.10.15         took part in "Hody" (folk festival) with girlfriend
2011               breakup with girlfriend, mild depression, antidep.
  2011.11.18         Minecraft is released
--------------------------------------------------------------------------------
2012.12            breakdown, depression, starting antidep., postponed school
--------------------------------------------------------------------------------
  2013.03.08         Miloš Zeman becomes the new president of Czech Republic
2013.06            holiday in Brela (Croatia) with family
2013.07            colored hair blonde
  2013.09.17         GTA V released
2013.10            released the game Mage Rage, won the fitst place at itnetwork.cz
--------------------------------------------------------------------------------
2014.02.07         IQ tested by Mensa (151)
2014.06.16         Bachelor's degree graduation at FIT BUT
2014.07            holiday in Brela (Croatia) with family (visited Amfiteatar at 43.5385055957982, 16.474188911763395, 2014.07.10 22:00)
2014.09.01         started computer graphics Master's program at FIT BUT
2014.09            took part in a charity run in Brno (Během Dětem)
2014.10            took part in game hackaton at FIT BUT
--------------------------------------------------------------------------------
2015.09            breakdown, depression, psychiatr., antidep., postponed school
--------------------------------------------------------------------------------
2016.08.01         got job at Code Creator (Brno) but failed to start (anxiety)
2016.08            holiday in Novi Vinodolski (Croatia) with family & cousins
2016.09.13         went to cousin's wedding as the best man (on scooter)
--------------------------------------------------------------------------------
2017.06.21         Master's degree graduation at FIT BUT
2017.07            holiday in Brela (Croatia) with parents, read the book "Free as In Freedom"
2017.08            applied for a job in Crea Vision (UH, IT), failed (anxiety)
2017.10            started contributing to OpenMW
2017               started a "job" distributing leaflets in the village (~7h/week)
2017.12.11         started the OpenMF (open source Mafia engine) project on GH, would go trending
--------------------------------------------------------------------------------
2018               transitioned to a "job" distributing newspaper in the morning (~12h/week)
2018               became vegetarian
2018.07            bought Arduboy
2018.08.15         bought Pokitto
--------------------------------------------------------------------------------
2019.03.18         started a "job" at factory reception (Hamé Babice, part time)
2019.09.01         published the manifesto Non-Competitive Society
2019.10            ?? ended the reception "job"
2019.11.13         went for a stay at psychiatric hospital (FN Brno)
--------------------------------------------------------------------------------
  2020.03.11         COVID-19 pandemic started
2020.02.26         returned from the psychiatric hospital, diagnosed AVPD
2020.05.07         admitted invalidity pension (1st degree) for AVPD
2020.05.11         first real job: cleaner in Fatra Napajedla (part time)
2020.11.20         released the game Anarch, a bit "successful"
--------------------------------------------------------------------------------
2021.06.16         left the cleaner job (4h/day)
2021.06.22 11:30   cut long hair (growing 2+ years)
2021.06.25         holiday in Brela, Croatia with mom
2021.07.12         started a job at a laundry (5h/day)
--------------------------------------------------------------------------------

TODO: Pokémon Sapphire, mandle, psychologist visits start, dog death dates,
  how I got scammed by the cellphone people at the bus stop, žampionárka

NOTES:

me at "slavnosti vína": https://www.youtube.com/watch?v=lpVQqj11_6Q 19:45
- game "Dablik" I played in Traplice is probably Litil Divil
