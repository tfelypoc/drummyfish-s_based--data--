Stuff about my family.

ancestor tree:

Miloslav Číž: 24.08.1990 (Zlín) - ???
  sňatek: 23.07.1988 (Uherské Hradiště)
  Miloslav Pšurný (Číž): 25.09.1966 (Uherské Hradiště) - ???
    Miloslav Pšurný: 1944 - ???
      Miloslav Pšurný: ??? - 28.08.1988 (Ostrožská Nová Ves)
        sňatek: 13.04.1921
        Antonín Pšurný: 06.06.1898 (Milokošť 152) - ???
          František Pšurný: 28.01.1860 (Milokošť 77) - ???
            Jan Pšurný: čtvrtník v Milokošti
              Mathias Pšurný:
              Katharina Lorenz:
                Matouš Lorenz:
            Kateřina Jochová: Milokošť
              Johan Joch:
              Anna Weseli:
                Sebastian Weseli:
          Mariana Bubeníková: 16.08.1864 - 26.02.1940 (Ostrožské Předměstí 29)
            Jan Bubeník: Ostrožské Předměstí
              Josef Bubeník:
              Elizabeth Laga:
                Mathias Laga:
            Mariana Ondračková: Ostrožská Nová Ves
              Antonín Ondračka: Ostrožská Nová Ves
              Anna Jurásek:
                Anton Jurásek:
        Antonie Karlíková: 19.05.1898 (Veselské Předměstí 423) - ???
          sňatek: 12.11.1890, Předměstí Veselí 356
          František Karlík: 21.03.1866 - ???, topič na dráze
            sňatek: 08.02.1860
            Antonín Karlík: 11.06.1829 - 03.03.1904, domkař ve Veselí 356
              sňatek: 09.02.1813
              Tomáš Karlík: Veselí 49
              Elizabeth Galářová:
            Marie Kozumplíková: Veselí 56
              Jiří Kozumplík: Veselí
              Johana Svatošová: Veselí
                Jakub Svatoš: Veselí
          Antonie Fingerová: 23.05.1867 (Předměstí Veselí 405) - ???
            Josef Finger: domkař ve Veselí
            Marie Fojtíková: Veselí
              Josef Fojtík: podsedník ve Veselí
      Anna Galušková: 10.10.1922 (Ostrožská Nová Ves 476) - 10.04.1984 (Ostrožská Nová Ves)
        Matůš Galuška: 20.09.1882 (Chylice 86) - ???
          sňatek: 16.08.1862
          Josef Galuška: 07.03.1839
            Jakob Galuška:
            Kateřina Widrman: Ostrožské Předměstí
          Mariana Zalubil: Ostrožská Nová Ves 136
            Jan Zalubil: Ostrožská Nová Ves
            Kateřina Lopata: Ostrožská Nová Ves
              Jan Lopata:
              Mariana ???:
        Alžběta Húsková: 29.06.1887 (Ostrožská Nová Ves 356)
          Bartoloměj Húsek: 02.11.1859 - ???
            Vavřinec Húsek: Ostrožská Nová Ves
            Apoléna Blaha: Hluk
          Mariana Kolísková:
    Emílie Vlachynská (Čížová): 29.09.1944 - ???      
      Antonín Vlachynský: 25.08.1907 - 27.09.1989
        sňatek: 28.05.1901
        František Vlachynský: 01.07.1878 - 1914 (Opole poblíž Lublína, Polsko, padl v 1. sv. v.)
          sňatek: 09.02.1875
          Jan Vlachynský: 1852 - ???
            František Vlachynský: Podolí
              Jan Vlachynský: 1796 - ???
                sňatek: 1778
                Jan Vlachynský:
                Marina ???:
              Magdalena ???: 1798 - ???
            Barbora Vaňková: Popovice 36
              Josef Vaněk:
          Rosálie ??? (Vichorcová): 1841 - ???, vdova
            Jan Tvrdoň:
            Barbora Hráčková:
        Rozálie Pašková: 16.03.1875 - 1965
          Jan Paška: ??? - 28.08.1878 (Podolí 98, objesil se v opilství)
            Antonín Paška: Podolí
            Apolónia Vaculinek: ??? - 14.09.1878 (Podolí)
          Mariana Perželková: Podolí 25
      Anežka Mikulcová (Vlachynská): 13.12.1918 - 30.12.2006 (Podolí 99)
        sňatek: 16.06.1908
        Metoděj Mikulec: 29.01.1885 - ???
          sňatek: 12.02.1866
          Tomáš Mikulec: 09.03.1846 - 09.01.1890 (Vlčnov 160)
            Šimon Mikulec: obecní pastýř ve Vlčnově
            Anna Křapa: ??? - 10.09.1889 (Vlčnov 213)
              Martin Křapa:
          Anna Pavelčíková: 30.03.1850 (Vlčnov 66) - 21.01.1897
            Franz Pavelčík: Vlčnov
            Riana Klabačková: Vlčnov
        Barbora Kryštofová: 04.12.1883 (Veletiny 42) - ???
          sňatek: 31.01.1883
          Josef Kryštof: 12.03.1854 - 09.01.1939 (Veletiny 42)
            Josef Kryštof: Veletiny
            Katarina Hruboš: ??? - 12.01.1862
              Vencl Hruboš: Veletiny
          Anna Piáčková: 22.07.1863 (Veletiny 16) - 06.03.1932 (Veletiny 42)
            sňatek: 1861
            Josef Piáček: 13.05.1843 - ???
              Georg Piáček: Veletiny
              Barbora ???: Havřice
            Magdalena Křivdová: Drslavice
              Jiří Křivda: Drslavice
              Anna Polášková: Hradčovice
                Josef Polášek: Hradčovice
  Vladimíra Daňková (Čížová): 22.01.1967 (Uherské Hradiště) - ???
    sňatek: 22.01.1966 (Babice 205)
    Josef Daněk: 14.04.1941 - ???
      Josef Daněk: 1914 (Kudlovice) - ???
        sňatek: 02.09.1912 (Spytihněv)
        Josef Daněk: 1881 - ???
          sňatek: 10.07.1877
          František Daněk: Bařice 19
            Ján Daněk: Lubná
              Jan Daněk: Lubná
              Anna Nedbal:
            Anna Kolouchová:
              Melchior Kolouch: Vrbka
          Anna Dvořanová: 27.01.1859 (Bařice 20) - ???
            Josef Dvořan: Bařice
              Franz Dvořan:
              Anna Bureš: Těšnovice
            Kateřina Roháčková:
              Josef Roháček: Břest (55)
              Brigita Motalík:
                Johan Motalík:
                Viktoria:
        Marie Janoušková: 1888 - ???
          Josef Janoušek: ??? - 08.01.1860 (Snovítky 27)
            Jan Janoušek:
              Cyril Janoušek:
              Anna Hrubý:
            Marie Růžičková (Janoušková):
          Terezie Masaříková: Snovítky
            Ondřej Masařík: Snovítky
            Mariana Šlampová: Snovítky
              Fabian Šlamp:
      Anežka Hořínková (Daňková):
        Klement Hořínek: 26.11.1885 - 1963 (Sušice 34)
          sňatek: 18.05.1870
          Klement Hořínek: 23.11.1844 - 20.09.1921 (Sušice 34)
            sňatek: 1829
            František Hořínek: Kudlovice 65, kovář
            Františka Jabůrková: Jankovice 24
              Martin Jabůrek: Jankovice
          Apolónia Škrabálková: 31.12.1848 - 25.10.1931
            František Škrabálek: 10.07.1818 - ??? (Sušice 7)
              Josef Škrabálek:
              Anna Škrabal:
                Johan Škrabal:
            Františka Sebeták: Sušice
              František Sebeták:
              Mariana Miták:
                Johan Miták:
    Marie Bartošíková (Daňková): 18.12.1945 - ???
      sňatek: 16.09.1901 (Babice)
      Martin Bartošík: 24.11.1910 - 13.09.1987
        Josef Bartošík: 17.03.1863 (Babice 68) - ???
          sňatek: 15.02.1860
          Josef Bartošík: 1833 - 23.08.1883
            sňatek: 12.02.1830 (Halenkovice)
            Vincenc Bartošík: 19.01.1800 (Babice 68) - ???
              Tomas Bartošík: ??? - 1829 (Babice)
              Johana Bičan: ??? - 1814 (Babice 86)
            Anna Juřenová: 23.02.1812 (Halenkovice 1) - ???
              Jan Juřena:
              Barbora Baštinec:
                Johan Baštinec:
          Terezie Janečková: 21.08.1838 - 21.05.1905 (Babice 75)
            Jan Janeček: 1803 (Babice) - 21.10.1890
              Anton Janeček: Babice
              Anna Komárek:
                Viktorin Komárek:
            Františka Býček:
              Franz Býček:
        Eleonora Janečková: 04.11.1875 (Babice 74) - ???
          sňatek: 07.02.1866
          Johan Janeček: 1836 - 09.06.1887 (Babice 75)
            Johan Janeček: ??? - 21.10.1890 (Babice)
              Anton Janeček:
              Anna Rozumek:
            Františka Býček: Babice
              Franz Býček: Babice
              Josefa Lahola:
                Johan Lahola: Spytihněv
          Josefa Pešlová: 16.02.1848 (Traplice 66) - 08.01.1937 (Babice)
            sňatek: 16.02.1841
            Šebestián Pešl: 23.07.1814 - ??? (Traplice 66)
              Johan Pešl: Traplice
              Anna Mikulík: Traplice
                Johan Mikulík:
            Johana Haša: 12.05.1820 - ??? (Traplice 64)
              Johan Haša: Traplice 64
              Barbora Hanák: Traplice
                Lorenz Hanák:
      Jaromíra Škrabalová (Bartošíková): 11.04.1921 - 30.01.2006
        František Škrabal: 01.08.1883 (Babice 79) - 30.11.1940 (Babice)
          sňatek: 02.06.1863 (Babice 67)
          Václav Škrabal: 1844 - 1899 (Babice 149)
            sňatek: 1889 (Huštěnovice)
            Jan Škrabal: 1810 - 1853 (Babice 2)
              sňatek: 1804 (Babice 2)
              Franz Škrabal: 1778
                Josef Škrabal:
                Marina ???:
              Matylda Třetiak: 1782 - 1820 (Babice 2)
                Anton Třetiak:
                Apolónia ???:
            Anna Bazalová: 1820 (Huštěnovice 17) - ???
              Matouš Bazala:
              Tekla Laholík:
                Frant Laholík:
          Matylda Kučerová: 1845 (Cerony) - 1914 (Babice 67)
            sňatek: 1839 (Cerony)
            František Kučera: 1820 - 1865 (Babice 67)
              Matouš Kučera: Huštěnovice
              Barbora Palánková: Huštěnovice
            Barbora Obdržálková: 1817 - ???
              Kaspar Obdržálek: 1786 (Uherské Hradiště) - 1831 (Cerony 10)
                Franz Obdržálek: Huštěnovice
                Barbara ???:
              Apolónia Olejnik: 1782 (Babice) - 1847 (Cerony 10)
                sňatek: 1779 (Babice)
                Martin Olejnik: 1753 (Babice) - ???
                  sňatek: 1751 (Babice)
                  Matheus Olejnik:
                  Juditha Bartošík:
                    Joanis Bartošík:
                Tereza Tomaštíková: 1757 (Kudlovice) - ???
                  Joanis Tomaštík: Kudlovice
                  Katarína ???:
        Rudolfína Rožková (Škrabalová): 07.11.1888 (Jalubí) - 08.02.1965 (Babice)
          sňatek: 26.10.1888 (Jalubí)
          Cyril Rožek: 09.03.1862 (Babice 83) - ???
            sňatek: 1860 (Babice)
            Franz Rožek: 03.02.1839 - 08.03.1892 (Babice 5)
              sňatek: 1808 (Spytihněv)
              Johan Rožek: 03.08.1804 (Babice 5) - ???
                sňatek: 1794 (Babice)
                Anton Rožek: 1780 - 1806 (Babice 5)
                  sňatek: 1776
                  Josephus Rožek:
                    Josephus Rožek:
                  Veronika Švehlák: 1759 (Spytihněv) - ???
                    Martin Švehlák:
                    Anna ???:
                Apolónia Varmuža: Huštěnovice 34
                  Johan Varmuža:
              Anna Snopková (Rožková): 24.09.1813 (Spytihněv) - 12.01.1883 (Babice)
                sňatek: 1797 (Kudlovice)
                Franz Snopek: Spytihněv 21
                Apolónia Kameníček (Snopková): 1780 (Kudlovice) - ???
                  sňatek: 1775
                  Martin Kameníček: Kudlovice
                  Anna Štěpaník: ??? - 1799
                    Jan Štěpaník:
            Josefa Mokrošová (Rožková): 26.04.1838 (Babice 94) - ???
              Jan Mokroš: 11.11.1813 - 20.08.1866 (Babice 94)
                sňatek: 1813 (Babice 94)
                Josef Mokroš: 1788 - 1825 (Babice 111)
                  sňatek: 1783
                  Šimon Mokroš: Babice 65
                    Jan Mokroš:
                  Barbara Rozumek:
                    Martin Rozumek:
                      sňatek: 1724 (Babice)
                      Laurent Rozumek: 1697
                        Václav Rozumek:
                        Kateřina ???:
                      Barbara Zuzanik:
                    Anna Haša: 1738 (Babice)
                      sňatek: 1721 (Babice)
                      Joanis Haša:
                      Elizabeta ???:
                Antónia Rozsypálková: 1778 (Topolná) - 1840 (Babice)
                  sňatek: 1776 (Topolná 30)
                  Franz Rozsypálek: ??? - 1817
                    Franz Rozsypálek:
                  Anna Čechmánek:
                    Martin Čechmánek:
              Apolónia Adamčíková (Mokrošová): 1815 (Huštěnovice) - 26.01.1867 (Babice)
                Anton Adamčík: 1788 (Huštěnovice 85) - 1847 (Huštěnovice)
                  sňatek: 1780
                  Gregor Adamčík: Huštěnovice 85
                  Apolónia Machalinek: Huštěnovice
                    Bartoloměj Machalinek:
                Katarína Grebeň: ??? - 1824 (Huštěnovice)
          Anna Obdržálková (Rožková): 16.08.1865 (Jalubí 80) - 21.03.1904 (Jalubí)
            sňatek: 28.01.1846 (Traplice)
            Josef Obdržálek: 11.01.1824 (Jalubí 80) - 17.01.1892 (Jalubí)
              sňatek: 29.01.1817 (Zlechov)
              Jan Obdržálek: ??? - 1862 (Jalubí)
                sňatek: 1769 (Jalubí)
                Anton Obdržálek: Jalubí 80
                Barbora Tomečková (Obdržálková): 01.03.1740 (Jalubí) - ???
                  Václav Tomeček: Jalubí
                  Kateřina ???:
              Alžběta Vávrová (Obdržálková): 28.06.1797 (Zlechov) - 26.10.1867 (Jalubí)
                sňatek: 1794 (Huštěnovice)
                Franz Vávra: 1775 (Zlechov) - ???
                  Mathias Vávra: ??? - 1799 (Zlechov 26)
                  Elizabeta ???:
                Apolónia Korvasová (Vávrová): 1776 (Huštěnovice 18) - 1856 (Zlechov)
                  sňatek: 1772 (Huštěnovice 89)
                  Václav Korvas: Huštěnovice
                  Katarína Bazala:
                    Bernard Bazala:
            Františka Rožková (Obdržálková): 07.08.1828 (Traplice 3) - 18.04.1899 (Traplice 3)
              sňatek: 06.02.1817 (Kudlovice)
              Josef Rožek: ??? - 15.03.1871 (Traplice 3)
                sňatek: 26.01.1785 (Traplice)
                Josef Rožek: Jalubí
                  Blažej Rožek: 1717 - 1769 (Jalubí)
                    Jan Rožek:
                    Anna ???:
                  Marina:
                Barbara Garážija: 1761 (Traplice) - 1813 (Traplice)
                  sňatek: 1759
                  Izidor Garážija: Traplice
                    Jakub Garážija: ??? - 1762
                    Alžběta ???:
                  Barbora Grebeň:
                    sňatek: 1730
                    Josef Grebeň:
                    Katarína Obdržálek:
              Anna Snopková (Rožková): 1789 (Kudlovice) - 1858 (Kudlovice)
                Franz Snopek: ??? - 1838 (Kudlovice 48)
                Barbara Plundrák (Snopková): Kudlovice 9
                  Johan Plundrák:
