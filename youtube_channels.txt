some nice youtube channels:

AlphaPhoenix
  PhD student with awesome ideas and experiments
  https://www.youtube.com/channel/UCCWeRTgd79JL0ilH0ZywSJA

Cool Worlds
  astrophysicist with extremely interesting and relaxing videos about space and alien life
  https://www.youtube.com/c/CoolWorldsLab
