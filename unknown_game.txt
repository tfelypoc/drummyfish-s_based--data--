This is a game I played with my bros when I was young but can't find. I've
already put considerable effort in searching for it, posted on reddit threads
but without success. If you have some info about this game, please let me know.

Anything here may be false as I was super small at the time and my memory may be
distorted, but here's what I remember:
    
Super old 2D DOS (probably) game from the 90s I used to play on Windows 3.1 I
think.

It was multiple-in-one type of game with like 10 subgames (the game selection
menu was horizontally placed pictures along the bottom side of the screen):

- One game where there was Yellow Submarine by Beatles MIDI melody playing as a
  music (we agree on this with my bros), I think you controlled a submarine in
  this game, seen from the side.
- In one you controlled a plane (my brother says maybe a helicopter) that kept
  flying from one side of the screen to the other and from top to bottom.
  There was a city underneath you and you had to drop bombs to wipe all the
  buildings so that you could land when you got all the way down.
- One game in which you controlled a soldier with some artillery canon (or
  something like that), it was top-down and you moved around the screen and kept
  shooting upwards at something.
- Also some snake-like game maybe? Not sure about this.

Platform(s): PC, prbably DOS (played on Windows 3.1 I think)
Genre: multiple-in-one (minigames)
Estimated year of release: about 1990 - 1997
Graphics/art style: 2D, colorful, small sprites
Notable gameplay mechanics: city bombing mini-game, top-down soldier with cannon
  game, submarine 2D game

I don't remember having to unlock the games, they were all playable from the
start.

