# Audtodokumentace psychického stavu

*Miloslav Číž, nar. 24.8.1990*

*poslední změna: 2020*

### dětství

- Šťastné, s oběma rodiči, nejstarší syn, dle okolí nadaný, budou na mě kladena velká očekávání. Matka začíná trpět depresí, dostane ID 3. stupně.

### 1997

- **ZŠ (Mařatice, Babice)**: Na ZŠ vzpomínám v dobrém. Zpětně si vybavuji občasné panické záchvaty při odloučení od rodiny/domova, stejně jako migrény s vizuálními aurami.

### 2006

- **SPŠ Zlín**: Špatné období, chalpecká třída, nerozumím si se spolužáky, mám ze školy stres. Jednou jsem byl odvezen sanitkou a **hospitalizován** s bolestmi břicha, příčina se nenašla (myslím, že jí byl stres).
- Jinak jsem "normální", smýšlím pravicově, jsem ambiciózní, daří se mi, chci rodinu.

### 2010
- **Přítelkyně**: První (a jediný) vztah, mladší (16), velmi intenzivní vztah, **nejšťastnější období** života. Nevinný vztah, žádný sexuální styk.
- **VUT Brno: informatika**: Škola mě baví, ale stresuje mě život v podnájmu se spolužáky ze SŠ.

### 2011

- **Rozchod**: ze strany přítelkyně, beru to velmi špatně, nesoustředím se, nastupují první **mírné deprese**, dr. Bychler předepisuje **escitalopram** a **neurol**.

### 2012

- Pomalu se vracím do normálu, **vysazuji antidepresiva**, hledám vztah, avšak neúspěšně.

### 2013

- Na Vánoce před Bc. státnicemi onemocním (jakoby chřipka), po týdnu si začínám všímat **problémů se zrakem** (zrnění, aury, unavenost očí, zákalky, "pulzování", ...), které se rychle zhoršují, přidává se **extrémní únava**, **vyhoření** (při pokusu o učení se dostavují extrémní bolesti hlavy, neujdu ani procházku) a **úzkosti** – jsem přesvědčený, že mám tzv. **visual snow** a že **oslepnu**, ale doktoři nic nenalézají. Upadám do **hluboké deprese**: nejím, nespím, hubnu, ztrácím zájem o vše, přemýšlím o **sebevraždě** (vlak) jako o možném řešení. Nastupuji k **psychiatru Konečnému** (Zlín), nasazuje mi **arketis**, **neurol** a **hypnogen**, **odkládám státnice** o rok. Jedná se do této doby o nejhorší období mého života, ale chci se vyléčit a vrátit zpět ke stejnému stylu života.

### 2014

- Stále v depresích, ale po roce volna jsem schopný naučit se na státnice a dokončit Bc. studium. Problémy se zrakem se zlepšily (avšak nikdy neodejdou úplně), po prázdninách už je nevnímám jako takový problém. Stejně tak únava, i když snížená výkonnost mi taky zůstává už pořád. Jsem nakonec schopný nastoupit na **magisterské studium**. Během studia se stav pozvolna zlepšuje až k "normálu".

### 2016

- Před posledním rokem VŠ zkouším nastupovat do **práce** v IT firmě. Díky lepšímu stavu se svolením psychiatra pomalu **vysazuji arketis**. To ale po několika týdnech vede ke zhoršování nálady a vyeskaluje v **extrémní panické záchvaty** v Brně: velmi rychlé **změny nálad** z euforie do naprosté deprese a rezignovanosti po několika minutách, pocení, třes, slabost, panika. **Extrémně snížená tolerance stresu**, jsem zmatený, utíkám z přednášek brečet na záchody. Odříkám nástup do práce. Úzkosti zkouším zahánět alkoholem, ale nefunguje to. Po několika dnech intenzivních změn nálad se **hroutím**, rodiče pro mě musí v noci přijet do Brna (autobusem nejsem schopen). Psychiatr znovu nasazuje **arketis**, **neurol** a **hypnogen**, znovu **odkládám státnice** o rok. Zažívám **nejhorší stavy v životě**: upadám opět do **hluboké deprese**, nejím, nespím, ztrácím zájem o cokoli i motivaci žít, naprosté vyčerpání těla i duše. Deprese a odevzdání se nyní navíc "perou" se záchvaty úzkostí a paniky, každý den je těžký boj, celý den proležím. Toto se začně mírně zlepšovat až cca. po půl roce, díky změně pohledu na život: zahazuji ambice, nenutím se už jít přes své hranice, v životě chci jen klid a mít radost z maličkostí.

### 2017

- Po zhroucení mám již **permanentně velmi sníženou toleranci stresu a výkonnost** a nemám již vysoké cíle (rodinu ani úspěch už nechci), nechci jen znovu skončit v depresi.
- **Nastupuji k psycholožce** (Mgr. Perničková, Zlín).
- **VŠ státnice**: Psychicky velmi náročné, musím pod prášky. 
- **hledání práce**:
  - Po škole zkouším nastoupit na IT pozici na částečný úvazek 
    (creavision, UH). Po zkušebním dni **se hroutím a odcházím**.
  - Jediné, co zvládnu, i když s potížemi, je **roznášní letáků** (brigádá): **6h týdně** (1 den v týdnu).
  - Po pár měsících přecházím na **roznos novin**: **1.5 h denně**.
  
### 2019

- Pomalá proměna **životní filozofie**: stávám se extrémně levicově smýšlejícím, **pacifistickým anarchistou** (s **komunistickými ideály**), strikně **proti kapitalismu**, vyznávajícím **životní minimalizmus**. Odsuzuji moderní "kapitalistickou" technologii, používám, prosazuji a programuji pouze **svobodný software** (https://cs.wikipedia.org/wiki/Svobodn%C3%BD_software) a vyznávám **svobodnou kulturu**.
- Díky psychoterapii nastupuji do nové práce: **vrátný (Hamé Babice)**: **brigáda na poloviční úvazek**. I když práce není moc náročná a mám ji vedle domu, velmi mě vyčerpává (neustálé napětí, kontakt s lidmi, odpor k nucené práci), nemám energii na běžné životní aktivity (socializace apod.).
- **Změna psychiatra: dr. Kašpárek**: začal jsem mít strach z chození k dr. Konečnému, protože mě nutí do aktivit, z nichž se hroutím.
- **Hospitalizace na psychiatrii fak. nemocnice Brno**: 4 měsíce, velmi **pozitivní zážitek**, můj stav se zde bez stresu pomalu zlepšuje, rozumím si s lidmi s psychickým hendikepem. Chodím na intenzivní **skupinovou terapii**.
- Diagnostikována **úzkostná/vyhýbavá porucha osobnosti** se sklony k afektivní poruše. **Arketis** je mi vyměněn za **tritico**.

### 2020

- **Covid pandemie**: Izolace mi nevadí, jsem v ní už roky. Smíšené pocity: rád vidím hroucení systému ale nechci, aby lidé trpěli.
- **Zhoršování nálady kvůli medikaci**: začínám mít pocity, jako když jsem r. 2016 vysadil aketis a pomalu mířím do deprese, po konzultaci s dr. Kašpárkem začínám brát obojí: **arketis** i **tritico**.
- **Sebezraňování**: Mírné sebezraňování při akutní úzkosti, nožem a žiletkou na rukou a jinde na těle, bez travalých jizev. (foto: https://upload.wikimedia.org/wikipedia/commons/0/04/Mild_Self_Harm_2.jpg).
- **Přiznání invalidního důchodu (1. stupeň)**: Přináší mi víc jistoty a možnost hledat si chráněné pracovní místo.
- Nástup do nové **chráněné práce: uklízeč Fatra Napajedla, 4. hod denně**: Zvládám, ale těžce, nemám čas/energii na socializaci a podobné aktivity. Po příchodu z práce bývám extrémně vyčerpaný a často spím, někdy až do večera.

### 2021

- Chodím na pomocnou skupinu jednou měsíčně (Labyrint, UH), k psycholožce (jednou za 2 týdny) a k psychiatrovi, beru Aketis a Tritico.
- Nejsem v hluboké depresi (vyhýbám se věcem, které k ní vedou), ale nejsem v životě šťastný, protože jsem utlačován systémem.
- Práce je pro mě zlo a utrpení beze smyslu a ničí mě.
- Smyslem života je mi vytváření a svobodné sdílení programů a umění na Internetu tak, aby pomáhali všem lidem, a rozvíjení své osobní filosofie anarchism/pacifismu/minimalismu (https://gitlab.com/drummyfish/my_writings/-/blob/master/non-competitive%20society.pdf).
- Rád bych se pomalu pokoušel o aktivity jako koníčky nebo lehkou socializaci, ale kvůli práci na to nemám sílu ani náladu. 
- Dodnes jsem se pořádně nedostal z rozchodu před 10 lety, o přítelkyni nechci nic vědět, bylo by to bolestné.
- Nástup do nové **chráněné práce: prádelna, 5. hod. denně**: Podobně jako u úklidu; zvládám, ale těžce.

